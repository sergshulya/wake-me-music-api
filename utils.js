module.exports = {
  notifyOnError(res, err) {
    if (err) {
      console.error(err)
      res.json({error: err})
    }
  },
  logObject(kind, object) {
    console.log(`Requested ${kind}`)
    console.dir(object)
  }
}

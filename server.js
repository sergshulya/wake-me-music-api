const express = require('express')
const bodyParser = require('body-parser')
const weatherRouter = require('./routers/weatherRouter')
const musicRouter = require('./routers/musicRouter')

const app = express()
app.use(bodyParser.json())
app.use('/api/weather', weatherRouter)
app.use('/api/music', musicRouter)

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*')
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
  next()
})

const port = process.env.PORT || 5000
app.listen(port)
console.log(`Server started listening on ${port}`)

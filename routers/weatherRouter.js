const properties = require('../properties')
const utils = require('../utils')
const express = require('express')
const router = express.Router()
const Forecast = require('forecast')

const forecast = new Forecast({
  service: 'darksky',
  key: properties.weatherToken,
  units: 'celcius',
  cache: true,
  ttl: {
    minutes: 27,
    seconds: 45
  }
})

router.route('/current')
.post((req, res) => {
  const location = req.body
  notifyOnEmptyLocation(res, location)

  forecast.get([location.latitude, location.longitude], (err, weather) => {
    utils.notifyOnError(res, err)
    utils.logObject('weather', weather.currently)
    res.json(weather.currently)
  })
})

function notifyOnEmptyLocation(res, location) {
  if (!location) {
    res.status(400).json({error: 'Please provide latitude and longitude'})
  }
}

module.exports = router

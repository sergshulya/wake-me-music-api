const properties = require('../properties')
const utils = require('../utils')
const express = require('express')
const router = express.Router()
const PlayMusic = require('playmusic')
const _ = require('lodash')
const pm = new PlayMusic()

pm.init({
  email: properties.email,
  password: properties.password
}, console.error)

const maxResults = 10

router.route('/:weather')
.get((req, res) => {
  pm.search(req.params.weather, maxResults, (err, data) => {
    utils.notifyOnError(res, err)
    const song = getRandomSong(data.entries)
    utils.logObject('song', song)

    pm.getStreamUrl(song.track.storeId, (err, streamUrl) => {
      utils.notifyOnError(res, err)
      res.json({streamUrl: streamUrl})
    })
  }, console.error)
})

function getRandomSong(entries) {
  // return entries.filter(entry => entry.type === '1').shift()
  return _.sample(entries.filter(entry => entry.type === '1'))
}

module.exports = router
